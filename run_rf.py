import sys
sys.path.insert(0, 'Utils/')
import reply_chats_module

import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support
from Feature_Extractor import Feature_Extractor
from sklearn.model_selection import KFold

from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import LinearSVC, SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier

NO_FOLDS = 10

if __name__ == "__main__":

    data = reply_chats_module.loadChats('Data/Chats/reply_annotations/', combine=True, load_pairs = True)
    pairs, labels = data
    print(len(pairs), len(labels), labels.count(0), labels.count(1))
    features = Feature_Extractor.compute_feature(pairs, [
                                                         Feature_Extractor.time_difference_feature,
                                                         Feature_Extractor.space_difference_feature,
                                                         Feature_Extractor.same_author_feature,
                                                         Feature_Extractor.mention_parent_feature,
                                                         Feature_Extractor.mention_child_feature,
                                                         ])

    # semantic features
    # f = np.load("Data/Chats/reply_annotations/features/ubuntu_features_v1_combined_original_dropout.npy")
    # features = np.hstack((features, f))

    acc = 0
    fscore = 0
    precision = 0
    recall = 0
    kf = KFold(n_splits=NO_FOLDS, shuffle=True)
    for train_index, test_index in kf.split(range(len(pairs))):

        train_pairs = []
        for index in train_index:
            train_pairs.append(pairs[index])
        train_labels = np.array(labels)[train_index].tolist()
        train_features = features[train_index]

        non_replies_train = train_labels.count(0)
        replies_train = train_labels.count(1)

        test_pairs = []
        for index in test_index:
            test_pairs.append(pairs[index])
        test_labels = np.array(labels)[test_index].tolist()
        test_features = features[test_index]
        non_replies_test = test_labels.count(0)
        replies_test = test_labels.count(1)

        # print(non_replies_train, replies_train)
        # print(non_replies_test, replies_test)


        # clf = DecisionTreeClassifier()
        # clf = LinearSVC()
        # clf = SVC(kernel='rbf')
        # clf = MLPClassifier(hidden_layer_sizes=(20,), solver="adam")
        clf = RandomForestClassifier(n_estimators=250, class_weight={0:1.0*non_replies_train/replies_train, 1:1})
        clf.fit(train_features, train_labels)
        print (clf.feature_importances_)
        predicted = clf.predict(test_features)
        acc_aux =  accuracy_score(test_labels, predicted, normalize=True)
        acc += acc_aux

        aux = precision_recall_fscore_support(test_labels, predicted, average='binary', pos_label=1)
        precision += aux[0]
        recall += aux[1]
        fscore += aux[2]


    print ("Accuracy = ", 1.0*acc/NO_FOLDS)
    print ("Precision = ", 1.0*precision/NO_FOLDS)
    print ("Recall = ", 1.0*recall/NO_FOLDS)
    print ("F1 Score = ", 1.0*fscore/NO_FOLDS)
