import json
import Dataset
import DataEntry
import sys

def timeDifference(timestamp1, timestamp2):
    # t1 - t2
    h1 = int(timestamp1.split(":")[0])
    m1 = int(timestamp1.split(":")[1])
    s1 = int(timestamp1.split(":")[2])

    h2 = int(timestamp2.split(":")[0])
    m2 = int(timestamp2.split(":")[1])
    s2 = int(timestamp2.split(":")[2])

    return (h1-h2)*60*60 + (m1-m2)*60 + (s1-s2)

def convertFormat(reply):

    nickname = reply["username"]
    utterance_id = reply["message_key"]
    utterance_timestamp = reply["time"]
    utterance_ref = -111
    utterance_text = reply["message"]

    utt = (nickname, utterance_id, utterance_timestamp, utterance_ref, utterance_text)
    return utt

def loadPairsWithAnnotation(data, annotation):

    # data = data[:8]
    pairs_annotations = []
    labels = []
    # print(data)
    # print(annotation)
    # print(len(data), len(annotation))
    for entry_index, entry in enumerate(data):
        message_index = entry['message_key']
        # if message_index != entry_index:
            # print("WRONG", message_index, entry_index)


        # find parents of current message
        if message_index >= len(annotation):
            parents = []
        else:
            parents = annotation[message_index]


        prev_index = entry_index - 1
        while True:
            if prev_index < 0:
                break

            prev_entry = data[prev_index]
            if timeDifference(entry["time"], prev_entry["time"]) > 129:
                break

            # add pair
            pair = [convertFormat(entry), convertFormat(prev_entry)]
            if prev_index in parents:

                labels.append(1)
            else:
                labels.append(0)

            pairs_annotations.append(pair)
            prev_index -= 1

    return pairs_annotations, labels

def loadDataWithAnnotation(data, annotation):

    data_annotation = []

    for index, p in enumerate(annotation):
        if len(p) == 0: # if we don't have parent
            continue
        else:
            parents = p

            question = data[index]
            answer_pool = []
            correct_answers = []
            prev_index = index - 1

            while True:
                if prev_index < 0:
                    break
                prev = data[prev_index]
                if timeDifference(question["time"],prev["time"]) > 129:
                    break

                if prev_index in parents:
                    correct_answers.append(convertFormat(prev))
                else:
                    answer_pool.append(convertFormat(prev))
                prev_index -= 1

            answer_pool.extend(correct_answers)
            answer_pool.reverse()
            correct_answers.reverse()
            question = convertFormat(question)

            de = DataEntry.DataEntry([question, correct_answers, answer_pool])
            data_annotation.append(de)

    data = Dataset.ChatLinksDataset(data_annotation)
    return data

def convertLinetoList(line):
    line = line[1:]
    line = line[:-1]
    lines = line.split("]")
    lines = list(map(lambda x: x.split(","), lines))
    l = []
    for st in lines[:-1]:
        laux = []
        for s in st:
            s = s.strip()
            s = s.replace("[","")
            if s == "":
                continue
            laux.append(int(s))
        l.append(laux)

    return l

def combineAnnotations(annotations):

    max_length = 0
    for annotation in annotations:
        if len(annotation) > max_length:
            max_length = len(annotation)


    parents = []
    for i in range(0, max_length):
        annotations_with_parent = []
        parent = []
        for annotation_index, annotation in enumerate(annotations):
            if i < len(annotation):
                if len(annotation[i]) != 0:
                    annotations_with_parent.append(annotation_index)
                    parent.extend(annotation[i])

        parents.append(list(set(parent)))

    f = open('Data/Chats/reply_annotations/annot4.json', 'w')
    json.dump(parents, f)
    f.write("\n")

def computeStatistics(annotation):
    # NOT FINAL
    print(annotation)
    counter_parents = [0 for _ in annotation]
    counter_child = [0 for _ in annotation]

    for index, value in enumerate(annotation):
        for parent in value:
            counter_child[parent] += 1
        if len(value) != 0:
            counter_parents[index] += 1

    print("PARENT =", 1.0 * sum(counter_parents) / len(annotation))
    print("CHILD = ", 1.0 * sum(counter_child) / len(annotation))

def loadChats(path, combine=True, annotated_eout=False, load_pairs=False):

    if annotated_eout == True:
        json_str = open(path + "messages_eout.json").read()
    else:
        json_str = open(path + "messages.json").read()
    data = json.loads(json_str)

    data = data[:524]

    if combine == False:
        with open(path + "annot1.json", 'r') as f:
            lines = f.read().splitlines()
            last_line = lines[-1]
            annotation1 = convertLinetoList(last_line)
            print(len(annotation1))
        with open(path + "annot2.json", 'r') as f:
            lines = f.read().splitlines()
            last_line = lines[-1]
            annotation2 = convertLinetoList(last_line)
            # print(len(annotation2))
        with open(path + "annot3.json", 'r') as f:
            lines = f.read().splitlines()
            last_line = lines[-1]
            annotation3 = convertLinetoList(last_line)
            # print(len(annotation3))

        if load_pairs == False:
            data_annotation1 = loadDataWithAnnotation(data, annotation1)
            data_annotation2 = loadDataWithAnnotation(data, annotation2)
            data_annotation3 = loadDataWithAnnotation(data, annotation3)

        else: # load pairs
            data_annotation1 = loadPairsWithAnnotation(data, annotation1)
            data_annotation2 = loadPairsWithAnnotation(data, annotation2)
            data_annotation3 = loadPairsWithAnnotation(data, annotation3)

        # computeStatistics(annotation1)

        return data_annotation1, data_annotation2, data_annotation3

    else:
        with open(path + "annot_combined.json", 'r') as f:
            lines = f.read().splitlines()
            last_line = lines[-1]
            annotation = convertLinetoList(last_line)

        # computeStatistics(annotation)
        if load_pairs == False:
            data = loadDataWithAnnotation(data, annotation)
        else:
            data = loadPairsWithAnnotation(data, annotation)
        return data

