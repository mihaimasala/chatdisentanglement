import string
import random
import utils
import math
import nltk
import numpy as np
from nltk.stem import WordNetLemmatizer
from nltk.stem import SnowballStemmer
from nltk.corpus import wordnet
from nltk.corpus import stopwords
import DataEntry
import re
import copy


class Dataset:

    def __init__(self, dataset, features=None):
        self.data = dataset
        self.features = features

    # features is a matrix of no_samples x candidate_count x features_size
    def add_features(self, features):
        self.features = features

    @property
    def no_samples(self):
        return len(self.data)

    #return batch_size question, positive and negative pairs and positive negative feature pairs
    def generateTrainingBatch(self, batch_size, total_per_sample, take_all_random=False):

        question_list = []
        positive_list = []
        negative_list = []


        positive_features = []
        negative_features = []



        while True:
            # select a random entry
            entry_index = random.randint(0, self.no_samples-1)
            question = self.data[entry_index].question

            # if we have no positive
            if len(self.data[entry_index].correct_answer) == 0:
                continue
            # add one positive
            positive_index = random.choice(range(0, len(self.data[entry_index].correct_answer)))
            correct_answer = self.data[entry_index].correct_answer[positive_index]

            # get gt features
            if self.features != None:
                correct_features = self.features[entry_index][positive_index]
            answer_pool = self.data[entry_index].answer_pool

            if take_all_random == False:
                if total_per_sample == -1:
                    # take all negative
                    take_negative_count = len(answer_pool) - len(self.data[entry_index].correct_answer)
                else:
                    take_negative_count = total_per_sample - len(self.data[entry_index].correct_answer)

                if take_negative_count == 0:
                    continue
                candidate_indexes = random.sample(list(range(len(self.data[entry_index].correct_answer),len(answer_pool))), take_negative_count)
                for candidate_index in candidate_indexes:
                    # append the question
                    question_list.append(question)

                    # append the correct answer
                    positive_list.append(correct_answer)

                    #append the candidate pool
                    negative_list.append(answer_pool[candidate_index])

                    # add positive and negative features
                    if self.features != None:
                        positive_features.append(correct_features)
                        negative_features.append(self.features[entry_index][candidate_index])

                    if len(question_list) == batch_size:
                        break

                if len(question_list) == batch_size:
                    break

        return [question_list, positive_list, negative_list], [positive_features, negative_features]

    #returns list of question and answers (of size batch_size X total_per_sample), features of answers, ground truth indexes, and split_indexes
    def generateTestingBatch(self, batch_size=-1, total_per_sample=-1, size_per_batch = 5):

        final_list = []
        question_list = []
        answer_list = []
        split_indexes = [0]
        gt_indexes = []
        features_list = []

        # use all questions
        if batch_size == -1:
            batch_size = self.no_samples

        batches_size = size_per_batch

        # select "batch_size" random indexes
        entry_indexes = random.sample(range(0, self.no_samples), batch_size)

        i = 0
        for entry_index in entry_indexes:
            current_gt_indexes  = []

            question = self.data[entry_index].question
            correct_answers = self.data[entry_index].correct_answer
            answer_pool = self.data[entry_index].answer_pool

            if total_per_sample == -1:
                take_negative_count = len(answer_pool) - len(correct_answers)
            else:
                take_negative_count = total_per_sample - len(correct_answers)

            # add ground truth
            for correct_answer_index, correct_answer in enumerate(correct_answers):
                question_list.append(question)
                answer_list.append(correct_answer)
                current_gt_indexes.append(correct_answer_index)
                if self.features != None:
                    features_list.append(self.features[entry_index][correct_answer_index])

            # add negative samples
            candidate_indexes = random.sample(list(range(len(correct_answers),len(answer_pool))), take_negative_count)
            for candidate_index in candidate_indexes:
                question_list.append(question)
                answer_list.append(answer_pool[candidate_index])

                if self.features != None:
                    features_list.append(self.features[entry_index][candidate_index])

            gt_indexes.append(current_gt_indexes)
            split_indexes.append(split_indexes[-1] + len(candidate_indexes) + len(correct_answers))

            i += 1

            if i == batches_size:
                i = 0
                final_list.append([[question_list, answer_list, answer_list], features_list, gt_indexes, split_indexes[1:]])
                question_list = []
                answer_list = []
                split_indexes = [0]
                gt_indexes = []
                features_list = []
        if question_list != []:
            final_list.append([[question_list, answer_list, answer_list], features_list, gt_indexes, split_indexes[1:]])
        return final_list

    def statistics(self, q=75, mean=True, median=True, percentile=True, unique=False, use_nltk=False):

        question_set = []
        correct_answer_set = []
        wrong_answer_set = []
        answer_set = []

        words_question = []
        words_correct_answer = []
        words_wrong_answer = []
        words_answer = []


        for entry_index, entry in enumerate(self.data):
            # print (entry_index)
            question = entry.question
            if use_nltk == True:
                words_q = nltk.word_tokenize(question)
            else:
                words_q = question.split(' ')
            words_question.append(len(words_q))
            if unique == True:
                if question not in question_set:
                    question_set.append(question_set)


            correct_answers = entry.correct_answer
            for correct_answer in correct_answers:
                if use_nltk == True:
                    words_ca = nltk.word_tokenize(correct_answer)
                else:
                    words_ca = correct_answer.split(' ')
                words_correct_answer.append(len(words_ca))
                words_answer.append(len(words_ca))

                if unique == True:
                    if correct_answer not in correct_answer_set:
                        correct_answer_set.append(correct_answer)
                    if correct_answer not in answer_set:
                        answer_set.append(correct_answer)

            for wrong_answer in entry.answer_pool:
                if use_nltk == True:
                    words_wa = nltk.word_tokenize(wrong_answer)
                else:
                    words_wa = wrong_answer.split(' ')
                words_wrong_answer.append(len(words_wa))
                words_answer.append(len(words_wa))
                if unique == True:
                    if wrong_answer not in wrong_answer_set:
                        wrong_answer_set.append(wrong_answer)
                    if wrong_answer not in answer_set:
                        answer_set.append(wrong_answer)

        if mean == True:
            print("MEAN")
            print("Q  = ", np.mean(words_question))
            print("CA = ", np.mean(words_correct_answer))
            print("WA = ", np.mean(words_wrong_answer))
            print("TA = ", np.mean(words_answer))

        if median == True:
            print("MEDIAN")
            print("Q  = ", np.median(words_question))
            print("CA = ", np.median(words_correct_answer))
            print("WA = ", np.median(words_wrong_answer))
            print("TA = ", np.median(words_answer))

        if percentile == True:
            print(q, "% PERCENTILE")
            print("Q  = ", np.percentile(words_question,q))
            print("CA = ", np.percentile(words_correct_answer,q))
            print("WA = ", np.percentile(words_wrong_answer,q))
            print("TA = ", np.percentile(words_answer,q))

        if unique == True:
            print("UNIQUE")
            print("Q  = ", len(question_set))
            print("CA = ", len(correct_answer_set))
            print("WA = ", len(wrong_answer_set))
            print("TA = ", len(answer_set))

    def get_wordnet_pos(self, treebank_tag):
        """
        return WORDNET POS compliance to WORDENT lemmatization (a,n,r,v)
        """
        if treebank_tag.startswith('J'):
            return wordnet.ADJ
        elif treebank_tag.startswith('V'):
            return wordnet.VERB
        elif treebank_tag.startswith('N'):
            return wordnet.NOUN
        elif treebank_tag.startswith('R'):
            return wordnet.ADV
        else:
            # As default pos in lemmatization is Noun
            return wordnet.NOUN

    def preprocess_sentence(self, sentence, use_lemmatizer, use_stemmer, filter_stop_words, lemmatizer, stemmer, stop_words):

        table = str.maketrans({key: None for key in string.punctuation})

        # transform to lower case
        sentence = sentence.lower()
        # trim whitespaces
        sentence = sentence.strip()
        # remove new lines
        sentence = sentence.replace('\n','')
        # remove tabs
        sentence = sentence.replace('\t', '')
        # remove links
        sentence = re.sub(r"http\S+", "", sentence)
        # remove consecutive whitespaces
        sentence = re.sub(r"\\s+", " ", sentence)
        # remove punctation
        sentence = sentence.translate(table)


        if use_lemmatizer == True or use_stemmer == True or filter_stop_words == True:
            words = nltk.word_tokenize(sentence)
            if use_stemmer == True:
                words = list(map(lambda x:stemmer.stem(x), words))
            elif use_lemmatizer == True:
                pos_tokens = nltk.pos_tag(words)
                words = ([lemmatizer.lemmatize(word, self.get_wordnet_pos(pos_tag)) for (word, pos_tag) in pos_tokens])

            if filter_stop_words == True:
                filtered_words = [word for word in words if word not in stop_words]
            else:
                filtered_words = words

            sentence = ' '.join(filtered_words)
            # remove whitespaces before punctuation marks
            # sentence = re.sub(r'\s([?.!,"](?:\s|$))', r'\1', sentence)
        return sentence

    def preprocess_data(self, use_lemmatizer=False, use_stemmer=False, filter_stop_words = False):

        stop_words = None
        if filter_stop_words == True:
            stop_words = stopwords.words('english')

        lemmatizer = None
        if use_lemmatizer == True:
            lemmatizer = WordNetLemmatizer()

        stemmer = None
        if use_stemmer == True:
            stemmer = SnowballStemmer("english")

        new_data = []

        for entry_index, entry in enumerate(self.data):
            # print (entry_index)
            q = entry.question
            nq = self.preprocess_sentence(sentence=q, use_lemmatizer=use_lemmatizer, use_stemmer=use_stemmer, filter_stop_words=filter_stop_words,lemmatizer=lemmatizer, stemmer=stemmer, stop_words=stop_words)

            nc = []
            for ca in entry.correct_answer:
                nca = self.preprocess_sentence(sentence=ca, use_lemmatizer=use_lemmatizer, filter_stop_words=filter_stop_words, use_stemmer=use_stemmer, lemmatizer=lemmatizer, stemmer=stemmer, stop_words=stop_words)
                nc.append(nca)

            nw = []
            for wa in entry.answer_pool:
                nwa = self.preprocess_sentence(sentence=wa, use_lemmatizer=use_lemmatizer, use_stemmer=use_stemmer, filter_stop_words=filter_stop_words, lemmatizer=lemmatizer, stemmer=stemmer, stop_words=stop_words)
                nw.append(nwa)

            de = DataEntry.DataEntry([nq, nc, nw])
            new_data.append(de)

        new_data = Dataset(new_data)
        return new_data

    # NEED TO CHANGE THIS!!!
    # def computeFeatures(data, output_filename):
    #
    #     print(len(data))
    #     max_value = 0
    #     conversation_features = [[] for _ in range(len(data))]
    #
    #     for conversation_index, conversation in enumerate(data):
    #         print(conversation_index, len(conversation))
    #         internal_list = [[] for _ in range(len(conversation))]
    #         for entry_index, entry in enumerate(conversation):
    #             windowSize = len(entry) - 1
    #             reply_utterance = entry[-1]
    #             target_id = reply_utterance[UTTERANCE_REF]
    #             with open("input.txt", "w") as filename:
    #
    #                 # write reply to file
    #                 filename.write("{0}\n".format(reply_utterance[UTTERANCE_TEXT]))
    #
    #                 # write window utterances to file
    #                 for index in range(0, len(entry) - 1):
    #                     utterance = entry[index]
    #                     if (utterance == None):
    #                         filename.write("\n")
    #                     else:
    #                         filename.write("{0}\n".format(utterance[UTTERANCE_TEXT]))
    #
    #             matrix = np.zeros((windowSize, 15))
    #             internal_count = 0
    #
    #             TYPES = ['intersection', 'spectrum', 'presence']
    #             for count in range(0, 5):
    #                 for t in TYPES:
    #                     cmd = 'java -classpath KernelString/src/ ComputeStringKernel {0} {1} {2} input.txt output.txt'.format(
    #                         t, count * 2 + 1, (count * 2 + 2))
    #                     call(cmd.split(), stdout=open("test.txt", "w"))
    #                     with open("output.txt", "r") as filename:
    #                         aux = np.array(
    #                             [list(test) for test in ([map(int, line.strip().split(' ')) for line in filename])])
    #
    #                     matrix[:, internal_count] = aux[1:, 0]
    #                     internal_count = internal_count + 1
    #
    #             internal_list[entry_index] = matrix
    #
    #             if len(matrix > 0):
    #                 max_value = max(max_value, np.amax(matrix))
    #
    #         conversation_features[conversation_index] = internal_list
    #
    #     for index1 in range(len(conversation_features)):
    #         # print (index1)
    #         for index2 in range(len(conversation_features[index1])):
    #             for index3 in range(len(conversation_features[index1][index2])):
    #                 conversation_features[index1][index2][index3] = conversation_features[index1][index2][
    #                                                                     index3] / max_value
    #
    #     with open(output_filename, 'wb') as fp:
    #         pickle.dump(conversation_features, fp)

    # convert data to have only one positive answer; add more samples!

    def convertTrain(self):

        new_data = []
        print (type(self.data), type(self.data[0]))
        for entry in self.data:

            if len(entry.correct_answer) > 1:

                new_question = entry.question
                for candidate_answer in entry.correct_answer:
                    new_correct_answer = [candidate_answer]
                    if len(entry.correct_answer) == len(entry.answer_pool):
                        new_answer_pool = list(filter(lambda x: x == candidate_answer, entry.answer_pool))
                    else:
                        new_answer_pool = list(filter(lambda x: x not in entry.correct_answer or x == candidate_answer, entry.answer_pool))
                    # print (entry.question)
                    # print (new_question)
                    # print (entry.correct_answer)
                    # print (new_correct_answer)
                    # print (entry.answer_pool)
                    # print (new_answer_pool)
                    de = DataEntry.DataEntry([new_question, new_correct_answer, new_answer_pool])
                    new_data.append(de)
            else:
                new_data.append(entry)
        print(type(new_data), type(new_data[0]))
        print (len(new_data))
        new_data = Dataset(new_data)
        print (new_data.no_samples)
        return new_data

    def generatePairs(self):

        pairs = []
        labels = []

        for entry in self.data:
            question = entry.question

            for candidate_answer in entry.answer_pool:
                p = [question, candidate_answer]
                pairs.append(p)
                if candidate_answer in entry.correct_answer:
                    labels.append(1)
                else:
                    labels.append(0)

        return pairs, labels


class ChatLinksDataset(Dataset):

    def __init__(self, list_of_DataEntry):
        Dataset.__init__(self, list_of_DataEntry)

    def convert_to_textDataset(self):
        count = 0
        dataset = []
        for entry in self.data:
            q = entry.question[4]

            ca = []
            for correct_answer in entry.correct_answer:
                ca.append(correct_answer[4])

            wa = []
            for wrong_answer_index, wrong_answer in enumerate(entry.answer_pool):
                if wrong_answer[4] in ca and wrong_answer_index >= len(ca):
                    count += 1
                    continue
                wa.append(wrong_answer[4])

            de = DataEntry.DataEntry([q, ca, wa])
            dataset.append(de)
        # print (count)
        return Dataset(dataset)

    def statistics(self, q=75, mean=True, median=True, percentile=True, unique=False, use_nltk=False):

        question_set = []
        correct_answer_set = []
        wrong_answer_set = []
        answer_set = []

        words_question = []
        words_correct_answer = []
        words_wrong_answer = []
        words_answer = []


        for entry_index, entry in enumerate(self.data):
            # print (entry_index)
            question = entry.question[4]
            # print (entry.question)
            if use_nltk == True:
                words_q = nltk.word_tokenize(question)
            else:
                words_q = question.split(' ')
            words_question.append(len(words_q))
            if unique == True:
                if question not in question_set:
                    question_set.append(question_set)


            correct_answers = entry.correct_answer
            for correct_answer in correct_answers:
                correct_answer = correct_answer[4]
                if use_nltk == True:
                    words_ca = nltk.word_tokenize(correct_answer)
                else:
                    words_ca = correct_answer.split(' ')
                words_correct_answer.append(len(words_ca))
                words_answer.append(len(words_ca))

                if unique == True:
                    if correct_answer not in correct_answer_set:
                        correct_answer_set.append(correct_answer)
                    if correct_answer not in answer_set:
                        answer_set.append(correct_answer)

            for wrong_answer in entry.answer_pool:
                wrong_answer = wrong_answer[4]
                if use_nltk == True:
                    words_wa = nltk.word_tokenize(wrong_answer)
                else:
                    words_wa = wrong_answer.split(' ')
                words_wrong_answer.append(len(words_wa))
                words_answer.append(len(words_wa))
                if unique == True:
                    if wrong_answer not in wrong_answer_set:
                        wrong_answer_set.append(wrong_answer)
                    if wrong_answer not in answer_set:
                        answer_set.append(wrong_answer)

        if mean == True:
            print("MEAN")
            print("Q  = ", np.mean(words_question))
            print("CA = ", np.mean(words_correct_answer))
            print("WA = ", np.mean(words_wrong_answer))
            print("TA = ", np.mean(words_answer))

        if median == True:
            print("MEDIAN")
            print("Q  = ", np.median(words_question))
            print("CA = ", np.median(words_correct_answer))
            print("WA = ", np.median(words_wrong_answer))
            print("TA = ", np.median(words_answer))

        if percentile == True:
            print(q, "% PERCENTILE")
            print("Q  = ", np.percentile(words_question,q))
            print("CA = ", np.percentile(words_correct_answer,q))
            print("WA = ", np.percentile(words_wrong_answer,q))
            print("TA = ", np.percentile(words_answer,q))

        if unique == True:
            print("UNIQUE")
            print("Q  = ", len(question_set))
            print("CA = ", len(correct_answer_set))
            print("WA = ", len(wrong_answer_set))
            print("TA = ", len(answer_set))

    # return batch_size question, positive and negative pairs and positive negative feature pairs
    def generateTrainingBatch(self, batch_size, total_per_sample, take_all_random=False):

            question_list = []
            positive_list = []
            negative_list = []

            positive_features = []
            negative_features = []

            while True:
                # select a random entry
                entry_index = random.randint(0, self.no_samples - 1)
                question = self.data[entry_index].question[4]
                correct_answer = self.data[entry_index].correct_answer[0][4]
                # get gt features
                if self.features != None:
                    correct_features = self.features[entry_index][0]
                answer_pool = self.data[entry_index].answer_pool

                if take_all_random == False:
                    if total_per_sample == -1:
                        # take all negative
                        take_negative_count = len(answer_pool) - 1
                    else:
                        take_negative_count = total_per_sample - 1

                    candidate_indexes = random.sample(list(range(1, len(answer_pool))), take_negative_count)
                    for candidate_index in candidate_indexes:
                        # append the question
                        question_list.append(question)

                        # append the correct answer
                        positive_list.append(correct_answer)

                        # append the candidate pool
                        negative_list.append(answer_pool[candidate_index][4])

                        # add positive and negative features
                        if self.features != None:
                            positive_features.append(correct_features)
                            negative_features.append(self.features[entry_index][candidate_index])

                        if len(question_list) == batch_size:
                            break

                    if len(question_list) == batch_size:
                        break

            return [question_list, positive_list, negative_list], [positive_features, negative_features]


    # returns list of question and answers (of size batch_size X total_per_sample), features of answers, ground truth indexes, and split_indexes
    def generateTestingBatch(self, batch_size=-1, total_per_sample=-1, size_per_batch=5):

            final_list = []
            question_list = []
            answer_list = []
            split_indexes = [0]
            gt_indexes = []
            features_list = []

            # use all questions
            if batch_size == -1:
                batch_size = self.no_samples

            batches_size = size_per_batch

            # select "batch_size" random indexes
            entry_indexes = random.sample(range(0, self.no_samples), batch_size)

            i = 0
            for entry_index in entry_indexes:
                current_gt_indexes = []

                question = self.data[entry_index].question[4]
                correct_answers = self.data[entry_index].correct_answer
                answer_pool = self.data[entry_index].answer_pool

                if total_per_sample == -1:
                    take_negative_count = len(answer_pool) - len(correct_answers)
                else:
                    take_negative_count = total_per_sample - len(correct_answers)

                # add ground truth
                for correct_answer_index, correct_answer in enumerate(correct_answers):
                    question_list.append(question)
                    answer_list.append(correct_answer[4])
                    current_gt_indexes.append(correct_answer_index)
                    if self.features != None:
                        features_list.append(self.features[entry_index][correct_answer_index])

                # add negative samples
                candidate_indexes = random.sample(list(range(len(correct_answers), len(answer_pool))),
                                                  take_negative_count)
                for candidate_index in candidate_indexes:
                    question_list.append(question)
                    answer_list.append(answer_pool[candidate_index][4])
                    if self.features != None:
                        features_list.append(self.features[entry_index][candidate_index])

                gt_indexes.append(current_gt_indexes)
                split_indexes.append(split_indexes[-1] + len(candidate_indexes) + len(correct_answers))

                i += 1

                if i == batches_size:
                    i = 0
                    final_list.append([[question_list, answer_list, answer_list], features_list, gt_indexes, split_indexes[1:]])
                    question_list = []
                    answer_list = []
                    split_indexes = [0]
                    gt_indexes = []
                    features_list = []
            if question_list != []:
                final_list.append([[question_list, answer_list, answer_list], features_list, gt_indexes, split_indexes[1:]])
            return final_list


    def preprocess_data(self, use_lemmatizer=False, use_stemmer=False, filter_stop_words = False):

        stop_words = None
        if filter_stop_words == True:
            stop_words = stopwords.words('english')

        lemmatizer = None
        if use_lemmatizer == True:
            lemmatizer = WordNetLemmatizer()

        stemmer = None
        if use_stemmer == True:
            stemmer = SnowballStemmer("english")

        new_data = []

        for entry_index, entry in enumerate(self.data):
            q = entry.question
            nqt = self.preprocess_sentence(sentence=q[4], use_lemmatizer=use_lemmatizer, use_stemmer=use_stemmer, filter_stop_words=filter_stop_words,lemmatizer=lemmatizer, stemmer=stemmer, stop_words=stop_words)
            nq = [q[0], q[1], q[2], q[3], nqt]

            nc = []
            for ca in entry.correct_answer:
                ncat = self.preprocess_sentence(sentence=ca[4], use_lemmatizer=use_lemmatizer, filter_stop_words=filter_stop_words, use_stemmer=use_stemmer, lemmatizer=lemmatizer, stemmer=stemmer, stop_words=stop_words)
                nca = [ca[0], ca[1], ca[2], ca[3], ncat]
                nc.append(nca)

            nw = []
            for wa in entry.answer_pool:
                nwat = self.preprocess_sentence(sentence=wa[4], use_lemmatizer=use_lemmatizer, use_stemmer=use_stemmer, filter_stop_words=filter_stop_words, lemmatizer=lemmatizer, stemmer=stemmer, stop_words=stop_words)
                nwa = [wa[0], wa[1], wa[2], wa[3], nwat]
                nw.append(nwa)

            de = DataEntry.DataEntry([nq, nc, nw])
            new_data.append(de)

        new_data = Dataset(new_data)
        return new_data