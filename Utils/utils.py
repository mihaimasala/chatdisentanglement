import numpy as np
import nltk
import random
import time
import string
import copy

# unk_token = np.random.uniform(-0.5, 0.5, (300,))
emb_dict = {}
c = 0
t = 0

def batchToEmbeddings(batch, embedding_model, max_sentence_length):
    global emb_dict

    batch_size = len(batch[0])
    question_text = batch[0]
    positive_text = batch[1]
    negative_text = batch[2]
    question_embs = []
    question_lengths = []

    positive_embs = []
    positive_lengths = []

    negative_embs = []
    negative_lengths = []

    for i in range(batch_size):

        if question_text[i] in emb_dict:
            question_embs.append(emb_dict[question_text[i]][0])
            question_lengths.append(emb_dict[question_text[i]][1])

        else:
            q_e, q_l = sentenceToEmbeddings(question_text[i], embedding_model, max_sentence_length)
            question_embs.append(q_e)
            question_lengths.append(q_l)
            emb_dict[question_text[i]] = [q_e, q_l]

        if positive_text[i] in emb_dict:
            positive_embs.append(emb_dict[positive_text[i]][0])
            positive_lengths.append(emb_dict[positive_text[i]][1])
        else:
            p_e, p_l = sentenceToEmbeddings(positive_text[i], embedding_model, max_sentence_length)
            positive_embs.append(p_e)
            positive_lengths.append(p_l)
            emb_dict[positive_text[i]] = [p_e, p_l]

        if negative_text[i] in emb_dict:
            negative_embs.append(emb_dict[negative_text[i]][0])
            negative_lengths.append(emb_dict[negative_text[i]][1])
        else:
            n_e, n_l = sentenceToEmbeddings(negative_text[i], embedding_model, max_sentence_length)
            negative_embs.append(n_e)
            negative_lengths.append(n_l)
            emb_dict[negative_text[i]] = [n_e, n_l]

    return np.array([question_embs, positive_embs, negative_embs]), [question_lengths, positive_lengths, negative_lengths]

def sentenceToEmbeddings(sentence, embedding_model, max_sentence_length):
    words = nltk.word_tokenize(sentence)
    # print (words)
    words = [word for word in words if word.isalnum()]
    # print (words)
    # words = sentence.split(' ')

    embeddings = []
    if len(words) == 0:
        # embeddings.append(unk_token)
        embeddings.append(np.random.uniform(-0.5, 0.5, embedding_model['dog'].shape))

    for word in words:
        embeddings.append(wordToEmbeddings(word, embedding_model))

    # trim embeddings
    if len(embeddings) >= max_sentence_length:
        print ("TRIMMING EMBEDDINGS!")
        embeddings = embeddings[:max_sentence_length]
        sequence_length = max_sentence_length
    # pad with zeros
    elif len(embeddings) < max_sentence_length:
        sequence_length = len(embeddings)
        to_pad = max_sentence_length - len(embeddings)
        for _ in range(to_pad):
            embeddings.append(np.zeros(embedding_model['dog'].shape))

    embeddings = np.array(embeddings)
    return embeddings, sequence_length

def wordToEmbeddings(word, embedding_model):
    if word in embedding_model:
        return embedding_model[word]
    else:
        # return unk_token
        return np.random.uniform(-0.5, 0.5, embedding_model['dog'].shape)

def computeAccuracy(prediction, ground_truth_indexes, split_indexes):
    correct_predictions = 0
    total_predictions = 0

    entry_predictions = np.split(prediction, split_indexes)[:-1]
    for index, scores in enumerate(entry_predictions):
        if len(scores) == 0:
            total_predictions += 1
            continue
        # find indexes of maximum value
        max_indexes = np.argwhere(scores == np.max(scores)).flatten().tolist()
        # print (max_indexes)

        #take a random index
        random_max_index = random.choice(max_indexes)
        # print (random_max_index)
        # print(scores, ground_truth_indexes[index], np.max(scores), max_indexes, random_max_index)

        if random_max_index in ground_truth_indexes[index]:
            correct_predictions += 1
        total_predictions += 1

    return 1.0*correct_predictions/total_predictions, correct_predictions

