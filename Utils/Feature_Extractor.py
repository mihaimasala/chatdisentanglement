import sys
sys.path.insert(0, '../Models/Baselines')
sys.path.insert(0, 'Utils/')
import reply_chats_module
import numpy as np
import utils
import gensim
import copy


embedding_model = None

class Feature_Extractor:


        def __init__(self, data=None):
            pass

        @staticmethod
        def check_data(data):

            #check pairs
            data_type = "pairs"
            for entry in data:
                if len(entry) != 2:
                    data_type = "not_pairs"
                    break

            if data_type == "pairs":
                return data_type

            #check full
            data_type = "full"
            for entry in data:
               if len(entry) != 3:
                   data_type = "not full"
                   break
            if data_type == "full":
                return data_type

            print("WRONG FORMAT!!!")
            sys.exit()

        @staticmethod
        def compute_feature(data, functions):

            data_type = None

            if data != None:
                data_type = Feature_Extractor.check_data(data)

            features = None
            if data_type == "pairs":

                for function_index, function in enumerate(functions):
                    local_features = []
                    for pair in data:
                        local_features.append(function(pair))
                    local_features = np.array(local_features)
                    local_features = np.reshape(local_features, (local_features.shape[0],-1))

                    if function_index == 0:
                        features = local_features
                    else:
                        features = np.hstack((features, local_features))

            return features

        @staticmethod
        def time_difference_feature(pair):
            utt1 = pair[0]
            utt2 = pair[1]
            return reply_chats_module.timeDifference(utt1[2], utt2[2])

        @staticmethod
        def space_difference_feature(pair):
            utt1 = pair[0]
            utt2 = pair[1]
            return utt1[1] - utt2[1]

        @staticmethod
        def same_author_feature(pair):
            utt1 = pair[0]
            utt2 = pair[1]
            return utt1[0] == utt2[0]

        @staticmethod
        def mention_parent_feature(pair):
            child = pair[0]
            parent = pair[1]
            return parent[0] in child[4] or parent[0].lower() in child[4]

        @staticmethod
        def mention_child_feature(pair):
            child = pair[0]
            parent = pair[1]
            return child[0] in parent[4] or child[0].lower() in parent[4]

        @staticmethod
        def parent_contains_question_feature(pair):
            return "?" in pair[1][4]


